#include <iostream>

using namespace std;

int main() {
    char name[20];
    char dessert[20];

    cout << "Enter your name:\n";
    cin.getline(name, 20);
    cout << "Enter your favourite dessert:\n";
    cin.getline(dessert, 20);

    cout << "I have some delicous " << dessert;
    cout << " for you, " << name << endl;
}
